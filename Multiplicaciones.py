from mblock import event
import time
import random
from pygame import sprite

@event.greenflag
def on_greenflag():
    sprite.set_variable('Puntaje', 0)
    time.sleep(2)
    sprite.say('Bienvenido!!!', 2)
    sprite.say('¿Cómo te llamas?', 2)
    answer = sprite.input('¿Como te llamas?')
    sprite.say(str('Hola ') + str(str(sprite.answer) + str(' a continuación vamos a jugar con multiplicaciones')), 3.5)
    time.sleep(2)
    sprite.say('Comencemos', 2)
    sprite.broadcast(str('mensaje1'))


@event.received('mensaje1')
def on_received():
    for count in range(5):
        sprite.set_variable('n1', random.randint(2, 12))
        sprite.set_variable('n2', random.randint(1, 12))
        answer = sprite.input(str('¿Cuánto es ') + str(str(sprite.get_variable('n1')) + str(str('x') + str(str(sprite.get_variable('n2')) + str('?')))))
        if sprite.answer == sprite.get_variable('n1') * sprite.get_variable('n2'):
            sprite.say('Correcto!!!', 2)
            v = sprite.get_variable('Puntaje')
            sprite.set_variable('Puntaje', v + 1)

        else:
            sprite.say('Incorrecto :c', 2)


    sprite.say(str('Tu nota es ') + str(str(sprite.get_variable('Puntaje') * 2) + str('/10')), 3.5)